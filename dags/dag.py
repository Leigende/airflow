from datetime import timedelta
from airflow.operators.python_operator import PythonOperator
import airflow
from airflow import DAG
from airflow.operators.bash_operator import BashOperator


default_args = {
    'owner': 'Leigende',
    'depends_on_past': False,
    'start_date': airflow.utils.dates.days_ago(2),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(days=1)
}

dag = DAG(
    'python',
    default_args=default_args,
    description='Python script',
    schedule_interval=timedelta(days=1),
)
